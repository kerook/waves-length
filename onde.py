#!/usr/bin/env python3


import math

d = float(input("Ciao!\nQual è la profondità? "))
T = float(input("Qual è il periodo? "))
dL = 0.1  # [m]
K = 9.81*(T**2)
L0 = K/math.tau
L1 = L0-dL
L2 = L0
print(f"L0 is: {L0:.4f}")

for itera in range(2, 1000):
    L1 -= dL
    L2 -= dL
    L3_1 = L0*math.tanh(math.tau*d/L1)
    L3_2 = L0*math.tanh(math.tau*d/L2)
    rapp_incr_1 = (L3_1-L1)/L1
    rapp_incr_2 = (L3_2-L2)/L2
    if abs(rapp_incr_1) > abs(rapp_incr_2):
        break

Lunghezza = L3_1 if abs(L3_1) < abs(L3_2) else L3_2
Lcontrol = L0*math.sqrt(math.tanh(math.tau**2*d/K))
ScartoL = Lcontrol-Lunghezza

print(f"L is: {Lunghezza:.4f} a {itera} "
      f"a {rapp_incr_1:.4f} a {rapp_incr_2:.4f}")
if 0.5 < d/Lunghezza:
    print("Si è in condizioni di Deep water")
elif 0.05 < d/Lunghezza:
    print("Si è in condizioni Transitional")
else:
    print("Si è in condizioni di Shallow Water")

print(f"Lcontrollo è {Lcontrol:.4f} e lo scarto "
      f"rispetto a quella iterata è di {ScartoL:.4f}")
