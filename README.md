# Waves length

This program is going to ask you:
* Depth d
* Period T

It'll return you:
* L0 associated with that period T
* L associated with that particular depth, as: L = gT2/2π * Th(2πd/L)
* The Classification of Water waves: Deep water / Trasitional / Shallow water
* The difference between the first L obtained and the L', as L' = gT2/2π * √Th[(2π)2d/(T2g)]
